use actix::prelude::*;

pub struct MyActor {
    pub count: usize,
}

impl Actor for MyActor {
    type Context = Context<Self>;
}
