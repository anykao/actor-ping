use actix::prelude::*;

pub struct Ping(pub usize);

impl Message for Ping {
    type Result = usize;
}
