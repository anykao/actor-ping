extern crate actix;
extern crate tokio;

use actix::prelude::*;
use tokio::prelude::Future;

mod actor;
mod message;

use actor::MyActor;
use message::Ping;

impl Handler<Ping> for MyActor {
    type Result = usize;

    fn handle(&mut self, msg: Ping, _ctx: &mut Context<Self>) -> Self::Result {
        self.count += msg.0;
        self.count
    }
}

fn main() {
    let system = System::new("test");
    let addr: Addr<Unsync, _> = MyActor { count: 10 }.start();
    let res = addr.send(Ping(10));
    // handle() returns tokio handle
    Arbiter::handle().spawn(
        res.map(|res| {
            println!("RESULT: {}", res == 20);
        }).map_err(|_| ()),
    );
    system.run();
}
